package main

import "fmt"

const (
	/* initial letters */
	letter1 = int32(23)
	letter2 = int32(22)
	letter3 = int32(28)
	letter4 = int32(19)
	/* number of initial letters */
	c = int32(33)
	N = int32(33)
)

var (
	hashTable map[int32]int32
)

/* function to find out "mod" number */
func modNumber(number int32) int32 {

	var (
		res int32
	)

	if number < 0 {
		res = (N - 1) + number
	} else {
		res = number
	}

	return  res

}

/* function to initialize hash table */
func initHashTable() {

	hashTable = make(map[int32]int32)
	hashTable[0] = 'a'
	hashTable[1] = 'б'
	hashTable[2] = 'в'
	hashTable[3] = 'г'
	hashTable[4] = 'ґ'
	hashTable[5] = 'д'
	hashTable[6] = 'e'
	hashTable[7] = 'є'
	hashTable[8] = 'ж'
	hashTable[9] = 'з'
	hashTable[10] = 'и'
	hashTable[11] = 'і'
	hashTable[12] = 'ї'
	hashTable[13] = 'й'
	hashTable[14] = 'к'
	hashTable[15] = 'л'
	hashTable[16] = 'м'
	hashTable[17] = 'н'
	hashTable[18] = 'о'
	hashTable[19] = 'п'
	hashTable[20] = 'р'
	hashTable[21] = 'с'
	hashTable[22] = 'т'
	hashTable[23] = 'у'
	hashTable[24] = 'ф'
	hashTable[25] = 'x'
	hashTable[26] = 'ц'
	hashTable[27] = 'ч'
	hashTable[28] = 'ш'
	hashTable[29] = 'щ'
	hashTable[30] = 'ь'
	hashTable[31] = 'ю'
	hashTable[32] = 'я'

}

/* function to write new row */
func writeRow(l1, l2, l3, l4, l5 int32) {

	var (
		symbol1 string
		symbol2 string
		symbol3 string
		symbol4 string
		symbol5 string
	)

	symbol1 = string(hashTable[l1])
	symbol2 = string(hashTable[l2])
	symbol3 = string(hashTable[l3])
	symbol4 = string(hashTable[l4])
	symbol5 = string(hashTable[l5])

	/* write new row */
	fmt.Println(symbol1 + symbol2 + symbol3 + symbol4 + symbol5)

}

/* entry point */
func main() {

	var (
		a, b      int32
	    i, i1     int32
		letterTr1 int32
		letterTr2 int32
		letterTr3 int32
		letterTr4 int32
	)

	/* initializa hash table */
	initHashTable()

	/* write initial row */
	fmt.Printf("Initial row: ")
	writeRow(letter1, letter2, letter3, letter4, letter3)

	/* go through all possible combinations */
	i = 2
	for i < N {
		i1 = 0
		for i1 < N {
			if N % i != 0 {
				a = i
				b = i1
				/* do transformation and write temporary result */
				fmt.Printf("a = %d, b = %d: ", a, b)
				letterTr1 = (a * letter1 - a * b) % c
				letterTr2 = (a * letter2 - a * b) % c
				letterTr3 = (a * letter3 - a * b) % c
				letterTr4 = (a * letter4 - a * b) % c
				letterTr1 = modNumber(letterTr1)
				letterTr2 = modNumber(letterTr2)
				letterTr3 = modNumber(letterTr3)
				letterTr4 = modNumber(letterTr4)
				writeRow(letterTr1, letterTr2, letterTr3, letterTr4, letterTr3)
			}
			i1++
		}
		i++
	}

}
